##### Access this site at
[docs.gitlab.com/debug](https://docs.gitlab.com/debug)

---

## About

This repo is dedicated to providing documentation and helpful commands
useful to GitLab Support Engineers in solving customer problems.

This site can include tricks, tips, and debugging guides for any tool used by
GitLab (such as Linux, Git, Ruby, Rails, Redis, Sidekiq, etc.).  It can also
include debugging steps taken to solve specific kinds of problems, including
scripts and/or console commands used.

##### This site is maintained by the GitLab Support team.  Community contributions welcome.

---

## Contributing

We welcome contributions.  Please read the following section to learn what
does and does not go in this repository.

#### What goes here

Any content used for debugging self-hosted GitLab instances.

#### What DOESN'T go here

Any content that describes how to use the software.  This content goes in the
docs of the appropriate repository (such as GitLab CE, EE, Omnibus, or GitLab
Runner).

#### Contribution Guidelines

- Keep all content under the `/content` directory.  
- Keep all images under the `/content/images` directory.
- Do not add any other files other than markdown and images
- Assign your merge request to a Support Engineer to approve.  If you are a
  Support Engineer, ask another Support Engineer to review your changes before
  merging.
