---
title: Project commands
date: 2018-03-06
tags: ["rails", "project", "ruby"]
---

Projects

<!--more-->

* UI Message - Failed to remove project repository

This project was scheduled for deletion, but failed with the following message: Failed to remove project repository. Please try again or contact administrator.

We can delete the projects marked as pending via the Rails console.

```ruby
#
# This section will list all the projects which are pending deletion
#
projects = Project.where(pending_delete: true)
projects.each do |p|
  puts "Project name: #{p.id}"
  puts "Project name: #{p.name}"
  puts "Repository path: #{p.repository.storage_path}"
end

#
# Assign a user (the root user will do)
#
user = User.find_by_username('root')

#
# For each project listed repeat these two commands
#

# Find the project, update the xxx-changeme values from above
project = Project.find_by_full_path('group-changeme/project-changeme')

# Delete the project
::Projects::DestroyService.new(project, user, {}).execute


# quit to exit console
quit
```

Next, run `sudo gitlab-rake gitlab:cleanup:repos` to finish.


