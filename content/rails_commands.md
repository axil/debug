---
title: Ruby and Rails commands
date: 2018-03-06
tags: ["rails", "linux", "console", "bash", "ruby"]
---

Ruby and Rails commands

<!--more-->

```ruby
# Using the rails console
rails console
rails c

# Using the rails runner, which doesn't enter the console
rails runner "RAILS_COMMAND"
rails r "RAILS_COMMAND"
```

```ruby
# Find a method containing a specific word
Array.methods.select { |m| m.to_s.include? "string" }
```

```ruby
# Change logging to debug level in the rails console
Rails.logger.level = 0
```