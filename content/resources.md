---
title: External Resources
tags: ["resources"]
---

External links to research different problems

<!--more-->

* [Testing with OpenSSL](https://www.feistyduck.com/library/openssl-cookbook/online/ch-testing-with-openssl.html)
* [Strace zine](https://jvns.ca/zines/#strace-zine)